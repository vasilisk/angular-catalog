import { Injectable } from '@angular/core';
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  users = [
    {username: 'admin', password: 'qwerty123'},
    {username: 'somebody', password: 'asdfg456'},
  ];

  isAuthenticated = false;

  constructor(private router: Router) { }

  checkAuth (username: string, password: string) {
    for (let user of this.users) {
      if (user.username == username && user.password == password) {
        this.isAuthenticated = true;
        this.router.navigate(['/people']);
      }
    }
  }

  logout () {
    this.isAuthenticated = false;
    this.router.navigate(['/login']);
  }
}
