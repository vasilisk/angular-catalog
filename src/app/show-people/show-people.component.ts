import {Component, DoCheck, OnInit} from '@angular/core';
import {AuthService} from "../auth.service";
import {PeopleService} from "../people.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-show-people',
  templateUrl: './show-people.component.html',
  styleUrls: ['./show-people.component.css']
})
export class ShowPeopleComponent implements DoCheck {

  people: any[] = [];
  id = -1;

  constructor(
    private authService: AuthService,
    private peopleService: PeopleService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngDoCheck(): void {
    if (this.authService.isAuthenticated) {
      this.people = this.peopleService.people;
      let id = this.route.snapshot.paramMap.get('id');
      if (id === null) {
        this.id = -1;
      } else {
        this.id = parseInt(id);
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

}
