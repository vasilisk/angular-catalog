import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginFormComponent} from "./login-form/login-form.component";
import {ShowPeopleComponent} from "./show-people/show-people.component";

const routes: Routes = [
  {path: 'login', component: LoginFormComponent},
  {path: 'people', component: ShowPeopleComponent},
  {path: 'people/:id', component: ShowPeopleComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
