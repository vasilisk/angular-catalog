import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  people = [
    {name: 'John Doe', dateOfBirth: '1985-06-07', address: 'Rīga', phone: '+371 10293847'},
    {name: 'Jane Miller', dateOfBirth: '1974-05-06', address: 'Kuldīga', phone: '+371 12345678'},
    {name: 'George Lucas', dateOfBirth: '1944-05-06', address: 'Ventspils', phone: '+371 23456789'},
  ];

  constructor() { }
}
